//import 'dart:html';

import 'package:flutter/material.dart';
//import 'package:flutter_auth_buttons/google.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: const MainPage(),
      routes: {
        '/user_page': (context) => const UserPage(),
        '/app_page': (context) => const AppPage(),
        // '/': (context) => MainPage(),
      },
    );
  }
}

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Обучающее приложение',
          style: TextStyle(
              fontSize: 30.0,
              color: Colors.black87,
              fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(Icons.menu),
        ),
        iconTheme: const IconThemeData(color: Colors.black54),
        // brightness: Brightness.light,
      ),
      body: _buildBody(context),
    );
  }
}

Widget _buildBody(context) {
  return SingleChildScrollView(
    child: Column(
      children: <Widget>[
        Align(alignment: Alignment.topCenter, child: _discription1()),
        _logoImage(),
        const SizedBox(
          height: 70.0,
        ),
        const Text(
          'Веб-версия приложения:',
          style: TextStyle(
            color: Colors.black,
            fontSize: 20.0,
          ),
        ),
        const SizedBox(
          height: 10.0,
        ),
        _buttonGoogleIn(context),
        const SizedBox(
          height: 20.0,
        ),
        _buttonsApp(context),
        const SizedBox(
          height: 90.0,
        ),
        Column(
          //mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            const Text(
              'О платформе',
              style: TextStyle(
                fontSize: 26.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(
              height: 30.0,
            ),
            const Text(
              'Прогрессивная технология обучения',
              style: TextStyle(
                color: Colors.black,
                fontSize: 20.0,
              ),
            ),
            _aboutPlatform(),
            _buttonsApp(context),
            const SizedBox(
              height: 120.0,
            ),
            const Text(
              'Содержание',
              style: TextStyle(
                color: Colors.black,
                fontSize: 20.0,
              ),
            ),
            _content(),

            // Text('Воздействие\nна различные\nсистемы восприятия'),
          ],
        ),
        _buttonsApp(context),
        const SizedBox(
          height: 120.0,
        ),
        const Text(
          'Разделы обучения',
          style: TextStyle(
            color: Colors.black,
            fontSize: 23.0,
          ),
        ),
        _sections(),
        _buttonsApp(context),
        const SizedBox(
          height: 120.0,
        ),
        const Text(
          'Наши пользователи',
          style: TextStyle(
            color: Colors.black,
            fontSize: 20.0,
          ),
        ),
        _users(),
        _buttonsApp(context),
        const SizedBox(
          height: 120.0,
        ),
        const Text(
          'ООО "Обучающее приложение" 2022',
          style: TextStyle(
            color: Colors.black,
            fontSize: 15.0,
          ),
        ),
      ],
    ),
  );
}

Column _discription1() {
  return Column(
    //mainAxisAlignment: MainAxisAlignment.center,
    //crossAxisAlignment: CrossAxisAlignment.center,
    children: const <Widget>[
      SizedBox(
        height: 32.0,
      ),
      // Text(
      //   'Говорящие уроки',
      //   style: TextStyle(
      //     fontSize: 32.0,
      //     fontWeight: FontWeight.bold,
      //  ),
      // ),
      // Divider(),
      // SizedBox(
      //   height: 38.0,
      //  ),
      Text(
        'Обучающие программы',
        style: TextStyle(
          color: Colors.black,
          fontSize: 22.0,
        ),
      ),
      Text(
        'Тренировка навыков',
        style: TextStyle(
          color: Colors.black,
          fontSize: 22.0,
        ),
      ),
      Text(
        'Обучение',
        style: TextStyle(
          color: Colors.black,
          fontSize: 22.0,
        ),
      ),
      SizedBox(
        height: 38.0,
      ),
    ],
  );
}

Image _logoImage() {
  return const Image(
    image: AssetImage('assets/Logo1.png'),
    fit: BoxFit.cover,
  );
}

MaterialButton _buttonGoogleIn(context) {
  return MaterialButton(
      color: Colors.white60,
      child: Container(
        width: 260.0,
        padding: const EdgeInsets.all(20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: const [
            Image(
              image: AssetImage('assets/google_logo.png'),
              height: 20.0,
            ),
            SizedBox(
              width: 10.0,
            ),
            Text("Войти через аккаунт Google "),
          ],
        ),
      ),
      onPressed: () {
        Navigator.pushNamed(context, '/user_page');
      });
}

Row _buttonsApp(context) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      MaterialButton(
        child: const Image(
          image: AssetImage('assets/appstore.png'),
          height: 50.0,
        ),
        onPressed: () {
          Navigator.pushNamed(context, '/app_page');
        },
      ),
      MaterialButton(
        child: const Image(
          image: AssetImage('assets/appgoogle.png'),
          height: 50.0,
        ),
        onPressed: () {
          Navigator.pushNamed(context, '/app_page');
        },
      ),
    ],
  );
}

Container _content() {
  return Container(
    // margin: const EdgeInsets.symmetric(vertical: 20.0),
    height: 200.0,
    width: 470,
    padding: const EdgeInsets.all(13.0),
    child: Column(
      // crossAxisAlignment: CrossAxisAlignment.center,
      // mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Row(
          children: <Widget>[
            SizedBox(
              height: 80.0,
              width: 210,
              child: Card(
                color: Colors.indigo[200],
                elevation: 30.0,
                child: const ListTile(
                  title: Text(
                    'Контроль качества',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  subtitle: Text('используемого контента'),
                ),
              ),
            ),
            SizedBox(
              height: 80.0,
              width: 220,
              child: Card(
                color: Colors.indigo[200],
                elevation: 30.0,
                child: const ListTile(
                  title: Text(
                    'Качество исполнения',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  subtitle: Text('учебных тренажёров'),
                ),
              ),
            ),
          ],
        ),
        Row(
          children: <Widget>[
            SizedBox(
              height: 80.0,
              width: 240,
              child: Card(
                color: Colors.indigo[200],
                elevation: 30.0,
                child: const ListTile(
                  title: Text(
                    'Участие преподавателей',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  subtitle: Text('в учебном процессе'),
                ),
              ),
            ),
            SizedBox(
              height: 80.0,
              width: 200,
              child: Card(
                color: Colors.indigo[200],
                elevation: 30.0,
                child: const ListTile(
                  title: Text(
                    'Широкий выбор',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  subtitle: Text('учебных занятий\n и тренажёров'),
                ),
              ),
            ),
          ],
        ),
      ],
    ),
  );
}

Container _aboutPlatform() {
  return Container(
    // margin: const EdgeInsets.symmetric(vertical: 20.0),
    height: 200.0,
    width: 470,
    padding: const EdgeInsets.all(13.0),
    child: Column(
      // crossAxisAlignment: CrossAxisAlignment.center,
      // mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Row(
          children: const <Widget>[
            SizedBox(
              height: 80.0,
              width: 200,
              child: Card(
                color: Colors.redAccent,
                elevation: 30.0,
                child: ListTile(
                  title: Text('Воздействие на различные'),
                  subtitle: Text(
                    'системы восприятия',
                    style: TextStyle(
                      fontSize: 16.0,
                      color: Colors.black87,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 80.0,
              width: 230,
              child: Card(
                color: Colors.tealAccent,
                elevation: 30.0,
                child: ListTile(
                  title: Text(
                    'Стимулирование\nмозговой деятельности',
                  ),
                  subtitle: Text(
                    'наводящими вопросами',
                    style: TextStyle(
                      fontSize: 16.0,
                      color: Colors.black87,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        Row(
          children: <Widget>[
            SizedBox(
              height: 80.0,
              width: 230,
              child: Card(
                color: Colors.limeAccent[400],
                elevation: 30.0,
                child: const ListTile(
                  title: Text(
                    'Использование методик',
                  ),
                  subtitle: Text(
                    'ускоренного обучения',
                    style: TextStyle(
                      fontSize: 16.0,
                      color: Colors.black87,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 80.0,
              width: 210,
              child: Card(
                color: Colors.green[400],
                elevation: 30.0,
                child: const ListTile(
                  title: Text('Индивидуальный подбор контента'),
                ),
              ),
            ),
          ],
        ),
      ],
    ),
  );
}

Container _users() {
  return Container(
    // margin: const EdgeInsets.symmetric(vertical: 20.0),
    height: 200.0,
    width: 230,
    padding: const EdgeInsets.all(13.0),
    child: Column(
      children: <Widget>[
        Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
          ),
          color: Colors.limeAccent[400],
          elevation: 20.0,
          child: const ListTile(
            title: Text(
              'Ученики',
              style: TextStyle(
                fontSize: 16.0,
                color: Colors.black87,
                fontWeight: FontWeight.bold,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
          ),
          color: Colors.indigo[200],
          elevation: 20.0,
          child: const ListTile(
            title: Text(
              'Учителя',
              style: TextStyle(
                fontSize: 16.0,
                color: Colors.black87,
                fontWeight: FontWeight.bold,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
          ),
          color: Colors.redAccent,
          elevation: 20.0,
          child: const ListTile(
            title: Text(
              'Кураторы',
              style: TextStyle(
                fontSize: 16.0,
                color: Colors.black87,
                fontWeight: FontWeight.bold,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ],
    ),
  );
}

Container _sections() {
  return Container(
    margin: const EdgeInsets.symmetric(vertical: 10.0),
    height: 200.0,
    child: SingleChildScrollView(
      // return ListView(
      padding: const EdgeInsets.all(13.0),
      scrollDirection: Axis.horizontal,
      //  controller: ScrollController(),
      //  physics: ScrollPhysics(),
      child: ListBody(
        mainAxis: Axis.horizontal,
        reverse: false,
        children: <Widget>[
          const SizedBox(
            height: 80.0,
            width: 120,
            child: Card(
              color: Colors.amber,
              elevation: 20.0,
              child: ListTile(
                subtitle: Text(
                  'Раздел 1',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 80.0,
            width: 120,
            child: Card(
              color: Colors.blueAccent,
              elevation: 30.0,
              child: ListTile(
                subtitle: Text(
                  'Раздел 2',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 80.0,
            width: 120,
            child: Card(
              color: Colors.greenAccent,
              elevation: 30.0,
              child: ListTile(
                subtitle: Text(
                  'Раздел 3',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
          SizedBox(
            height: 80.0,
            width: 120,
            child: Card(
              color: Colors.cyan[300],
              elevation: 30.0,
              child: const ListTile(
                subtitle: Text(
                  'Раздел 4',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
        ],
      ),
    ),
  );
}

class UserPage extends StatelessWidget {
  const UserPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        // child: Center(
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.center,
          // crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            const SizedBox(
              height: 32.0,
            ),
            const Text(
              'Обучающее приложение',
              style: TextStyle(
                fontSize: 32.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            // Divider(),
            const SizedBox(
              height: 38.0,
            ),
            const Text(
              'Спасибо за тестирование.',
              style: TextStyle(
                color: Colors.black,
                fontSize: 18.0,
              ),
              textAlign: TextAlign.left,
            ),
            const SizedBox(
              height: 20.0,
            ),
            const Text(
              'Вы учтены в системе под именем:',
              style: TextStyle(
                color: Colors.black,
                fontSize: 18.0,
              ),
              textAlign: TextAlign.left,
            ),
            const Text(
              'Пользователь 1',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.black,
                fontSize: 18.0,
              ),
              textAlign: TextAlign.left,
            ),

            const Padding(
              padding: EdgeInsets.all(10.0),
              child: Text(
                'Возможность корректировки информации о себе будет добавлена в систему в ближайшее время',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 18.0,
                ),
                textAlign: TextAlign.left,
              ),
            ),
            const Padding(
              padding: EdgeInsets.all(50),
              child: Image(
                image: AssetImage('assets/man1.png'),
                fit: BoxFit.cover,
              ),
            ),
            const Text(
              'Связаться с нами:',
              style: TextStyle(
                color: Colors.black,
                fontSize: 21.0,
              ),
            ),
            const Image(
              image: AssetImage('assets/networks.png'),
              height: 90.0,
              //  fit: BoxFit.cover,
            ),
            const SizedBox(
              height: 38.0,
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
                // Navigator.pushNamed(context, '/');
              },
              child: const Text('Выйти из системы'),
              style: ButtonStyle(
                  //  fixedSize: MaterialStateProperty<Size>(60.0),
                  backgroundColor:
                      MaterialStateProperty.all<Color>(Colors.grey)),
            ),
            const SizedBox(
              height: 48.0,
            ),
            _logoSmallText(),
          ],
        ),
      ),
    );
    // );
  }
}

class AppPage extends StatelessWidget {
  const AppPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              const SizedBox(
                height: 32.0,
              ),
              const Text(
                'Обучающее приложение',
                style: TextStyle(
                  fontSize: 32.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              // Divider(),
              const SizedBox(
                height: 38.0,
              ),
              const Text(
                'Уважаемые пользователи!',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 18.0,
                ),
              ),
              const SizedBox(
                height: 20.0,
              ),
              const SizedBox(
                height: 130.0,
                width: 400.0,
                child: Text(
                  'В настоящее время приложение находится на этапе разработки.\n\nСейчас доступна в режиме тестирования упрощенная веб-версия приложения.',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 18.0,
                  ),
                ),
              ),
              const SizedBox(
                height: 18.0,
              ),
              const Text(
                'Приносим свои извинения',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 18.0,
                ),
              ),
              const SizedBox(
                height: 20.0,
              ),
              // Image.asset('assets/logo2020.png'),
              const Image(
                image: AssetImage('assets/cat.png'),
                height: 350.0,
                //  fit: BoxFit.cover,
              ),
              const SizedBox(
                height: 38.0,
              ),
              const Text(
                'Вход в веб-версию приложения',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 18.0,
                ),
              ),
              const SizedBox(
                height: 10.0,
              ),
              _buttonGoogleIn(context),
              const SizedBox(
                height: 38.0,
              ),
              ElevatedButton(
                onPressed: () {
                  //  Navigator.pushNamed(context, '/main_page');
                  Navigator.pop(context);
                },
                child: const Text('Назад'),
                style: ButtonStyle(
                    //  fixedSize: MaterialStateProperty<Size>(60.0),
                    backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.grey)),
              ),
              const SizedBox(
                height: 48.0,
              ),
              _logoSmallText()
            ],
          ),
        ),
      ),
    );
  }
}

Row _logoSmallText() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: const <Widget>[
      Image(
        image: AssetImage('assets/logo1.png'),
        height: 40.0,
        //  fit: BoxFit.cover,
      ),
      SizedBox(
        height: 30.0,
        width: 20.0,
      ),
      Text(
        'ООО "Обучающее приложение"',
        style: TextStyle(
          color: Colors.black,
          fontSize: 15.0,
        ),
      ),
    ],
  );
}
